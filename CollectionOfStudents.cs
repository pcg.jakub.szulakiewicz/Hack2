﻿using System.Collections.Generic;
using System.Linq;

public class CollectionOfStudents : List<Student>
{
    public int MatchedCount => this.Count(st => st.HasPlace);

    public Student GetById(int id) => this.FirstOrDefault(st => st.Identifier == id);
}