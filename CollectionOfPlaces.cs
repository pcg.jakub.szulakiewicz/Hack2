﻿using System;
using System.Collections.Generic;
using System.Linq;

public class CollectionOfPlaces : List<Place>
{
    internal Place GetById(string placeId) => this.FirstOrDefault(pl => pl.Identifier == placeId);
}