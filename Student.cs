﻿
using System;

public class Student
{
    public int Identifier { get; }

    public CollectionOfPlaces PotentialPlaces { get; } = new CollectionOfPlaces();

    public Place SelectedPlace { get; private set; }
    public bool HasPlace => SelectedPlace != null;

    public Student(int identifier)
    { 
        Identifier = identifier;
        Distance = -1;
    }

    public void SelectPlace(Place place)
    {
        if (place.IsMatched) place.MatchedStudent.UnselectPlace();

        this.SelectedPlace = place;
        place.MatchedStudent = this;
    }

    public void UnselectPlace()
    {
        if (this.HasPlace) this.SelectedPlace.MatchedStudent = null;
        this.SelectedPlace = null;
    }

    internal void AddPotentialPlace(Place place)
    {
        PotentialPlaces.Add(place);
        place.AddPotentialStudent(this);
    }

    public int Distance;
    public bool IsVisited;
}