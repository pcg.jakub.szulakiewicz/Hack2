﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Hack2
{
    class Program
    {
        static CollectionOfStudents students = new CollectionOfStudents();
        static CollectionOfPlaces places = new CollectionOfPlaces();

        static void Main(string[] args)
        {
            LoadSample();
            LoadSamle2();
            //LoadData();

            var edges = students.Sum(student => student.PotentialPlaces.Count);
            var vertices = students.Count + places.Count;

            var complexity = edges * Math.Sqrt(vertices);

            Console.WriteLine($"Edges: {edges}, Vertices: {vertices}, Complexity: {complexity}");
            while (true)
            {
                //Console.WriteLine($"0. Matched students: {students.MatchedCount}");
                BfsPhase();
                //Console.WriteLine($"1. Matched students: {students.MatchedCount}");
                var cont = DfsPhase();
                foreach(var student in students)
                {
                    Console.WriteLine($"      {student.Distance} S-{student.Identifier}   ->    {student?.SelectedPlace?.Identifier}");
                }
                Console.WriteLine($"2. Matched students: {students.MatchedCount}");


                if (!cont) break;
            }
        }

        private static void LoadSample()
        {
            /// Tworze proste testowe dane jak w przykładzie :https://media.geeksforgeeks.org/wp-content/cdn-uploads/HopcroftKarp1.png
            /// 


            var u0 = new Place("u0");
            var u1 = new Place("u1");
            var u2 = new Place("u2");
            var u3 = new Place("u3");

            var v0 = new Student(0);
            v0.AddPotentialPlace(u1);

            var v1 = new Student(1);
            v1.AddPotentialPlace(u0);
            v1.AddPotentialPlace(u2);
            v1.AddPotentialPlace(u3);

            var v2 = new Student(2);
            v2.AddPotentialPlace(u0);

            var v3 = new Student(3);
            v3.AddPotentialPlace(u3);

            students.Add(v0);
            students.Add(v1);
            students.Add(v2);
            students.Add(v3);

            places.Add(u0);
            places.Add(u1);
            places.Add(u2);
            places.Add(u3);

        }

        private static void LoadSample2()
        {
            /// Tworze proste testowe dane jak w przykładzie :http://www.rafalnowak.pl/wiki/images/5/53/Bipartite_matching.jpeg
            /// Zakładam, że górne węzły to studenci 1,2,3,4 a dolne to miejsca A,B,C,D
            /// 


            var pA = new Place("A");
            var pB = new Place("B");
            var pC = new Place("C");
            var pD = new Place("D");

            var s1 = new Student(1);
            s1.AddPotentialPlace(pA);

            var s2 = new Student(2);
            s2.AddPotentialPlace(pA);
            s2.AddPotentialPlace(pB);
            s2.AddPotentialPlace(pD);

            var s3 = new Student(3);
            s3.AddPotentialPlace(pB);
            s3.AddPotentialPlace(pC);

            var s4 = new Student(4);
            s4.AddPotentialPlace(pC);

            students.Add(s1);
            students.Add(s2);
            students.Add(s3);
            students.Add(s4);

            places.Add(pA);
            places.Add(pB);
            places.Add(pC);
            places.Add(pD);
        }

        static void BfsPhase()
        {
            ///1.wszystkim mężczyznom nadaj odległość - 1
            students.ForEach(st => st.Distance = -1);

            ///Teraz
            /// 2.niech Q będzie pustą kolejką

            var Q = new Queue<Student>();

            /// 3.wszystkich nieskojarzonych jeszcze mężczyzn wstaw do kolejki Q oraz nadaj im odległość 0

            foreach (var student in students.Where(st => !st.HasPlace))
            {
                student.Distance = 0;
                Q.Enqueue(student);
            }

            ///A teraz zwyczajny BFS
            /// 4.dopóki Q nie jest pusta

            while (Q.Any())
            {
                /// 4.1.wyciągnij pierwszego mężczyznę x z kolejki Q

                var student = Q.Dequeue();
                /// 4.2.dla każdej kobiety y, którą zna x wykonaj:

                foreach (var place in student.PotentialPlaces)
                {
                    /// jeśli y jest skojarzona z mężczyzną z

                    var competitor = place.MatchedStudent;
                    if (competitor!=null && competitor.Distance==-1)
                    /// oraz jeśli z ma przypisaną odległość - 1(nie był jeszcze odwiedzony), to
                    {
                        ///nadaj z odległość o jeden większą niż ma odległość x
                        competitor.Distance = student.Distance + 1;
                        ///wstaw z do kolejki Q
                        Q.Enqueue(competitor);
                    }
                }
            }
        }

        static bool DfsPhase()
        {
            var result = false;
            ///1.każdego mężczyznę oznacz jako nieodwiedzonego
            students.ForEach(st => st.IsVisited = false);


            ///Teraz będziemy wywoływać serię DFS'ów w każdym nieodwiedzonym jeszcze wierzchołku
            foreach(var student in students)
            {
                if (student.IsVisited) continue;

                ///2.dla każdego mężczyzny x wykonaj:
                ///    jeśli x nie jest skojarzony, to wykonaj DFS(x)

                if (!student.HasPlace)
                {
                    var dfsResult = Dfs(student);
                    if (dfsResult) result = true;
                }
            }

            return result;
        }

        static bool Dfs(Student student)
        {
            ///zaznacz mężczyznę x jako odwiedzonego

            student.IsVisited = true;

            ///dla każdej znanej mu kobiety y wykonaj:
            foreach (var place in student.PotentialPlaces)
            {
                var ispossible = student.PotentialPlaces.Where(pl => !pl.IsMatched);
                ///jeśli y nie jest skojarzona, to skojarz x z y i zwróć true
                if (!place.IsMatched)
                {
                    student.SelectPlace(place);
                    return true;
                }

                ///niech z oznacza mężczyznę, który jest skojarzony z y

                var competitor = place.MatchedStudent;

                //jeśli z nie jest jeszcze odwiedzony oraz odległość z jest o jeden większa niż odległość x, to
                if (!competitor.IsVisited && competitor.Distance == student.Distance + 1)
                {
                    ///wykonaj DFS(z)
                    var dfsResult = Dfs(competitor);

                    ///jeśli wynik to true to skojarz x z y i zwróć true

                    if (dfsResult)
                    {
                        student.SelectPlace(place);
                        return true;
                    }

                }
            }

            ///zwróć false

            return false;
        }

        private static void LoadData()
        {
            using (var reader = new StreamReader("source_data.csv"))
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine().Split(";");

                    var studentId = int.Parse(line[0]);
                    var specialisationId = int.Parse(line[1]);
                    var maxCapacity = int.Parse(line[2]);


                    var student = students.GetById(studentId);
                    if (student == null)
                    {
                        student = new Student(studentId);
                        students.Add(student);
                    }

                    for (int i=0;i<maxCapacity;i++)
                    {
                        string placeId = $"{specialisationId}-{i}";
                        var place = places.GetById(placeId);
                        if (place == null)
                        {
                            place = new Place(placeId);
                            places.Add(place);
                        }

                        place.AddPotentialStudent(student);
                        student.AddPotentialPlace(place);
                    }
                }
        }
    }
}
