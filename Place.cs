﻿using System;

public class Place
{
    public string Identifier { get; }

    public CollectionOfStudents PotentialStudents { get; } = new CollectionOfStudents();
    public Place(string identifier)
    {
        Identifier = identifier;
    }

    public Student MatchedStudent { get; internal set; }
    public bool IsMatched => MatchedStudent != null;

    internal void AddPotentialStudent(Student student) => PotentialStudents.Add(student);
}