Próba implementacji algorytmu H-K.

Na podstawie:
http://www.rafalnowak.pl/wiki/index.php?title=Algorytm_Hopcrofta-Karpa

Działa poprawnie dla przykładów:
https://media.geeksforgeeks.org/wp-content/cdn-uploads/HopcroftKarp1.png
http://www.rafalnowak.pl/wiki/images/5/53/Bipartite_matching.jpeg

(rzeba podmienić wywołanie LoadData() na LoadSample() lub LoadSample2() - szczególnie na tym drugim przykładzie ładnie widać, jak algorytm najpierw rozpinaniektóre połączenia, żeby później stworzyć inne).

Niestety, dla danych rzeczywistych albo wpada w pętlę, albo działa BARDZO długo. 
Przypuszczam, że coś gdzieś pokrzaczyłem, ale nawet nie mam pomysłu na to, jak śledzić zmiany przy tak dużej próbce danych.
W każdym razie progres zatrzymuje się na wyniku 662 połączenia.